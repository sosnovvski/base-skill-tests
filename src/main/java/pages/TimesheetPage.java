package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class TimesheetPage extends BasePage implements IPage {
    @FindBy(className = "bi-chat-dots-fill")
    private WebElement commentIcon;

    @FindBy(xpath = "//textarea")
    private WebElement commentTextarea;

    public TimesheetPage(WebDriver driver) {
        super(new WebDriverWait(driver, Duration.ofSeconds(10)), driver);
        PageFactory.initElements(driver, this);
    }

    @Override
    public boolean isPageDisplayed() {
        return webDriverWait.until(webDriver -> commentIcon.isDisplayed());
    }

    public void clickCommentIcon() {
        commentIcon.click();
    }

    public String fetchCommentText() {
        webDriverWait.until(ExpectedConditions.attributeToBe(commentTextarea, "value", "Leadership Development"));
        return commentTextarea.getAttribute("value");
    }
}
