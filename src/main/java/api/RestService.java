package api;

public class RestService {
    private final IBookingService bookingService;
    private final IAuthService authService;

    public static RestService getRestService() {
        return new RestService();
    }

    public RestService() {
        bookingService = new BookingService();
        authService = new AuthService();
    }

    public IBookingService getBookingService() {
        return bookingService;
    }

    public IAuthService getAuthService(){
        return authService;
    }
}
