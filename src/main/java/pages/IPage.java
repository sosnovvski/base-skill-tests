package pages;

public interface IPage {
    boolean isPageDisplayed();
}
