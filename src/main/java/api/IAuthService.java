package api;

public interface IAuthService {
    String getToken();
}
