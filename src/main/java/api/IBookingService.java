package api;

import api.model.Booking;
import io.restassured.response.Response;

import java.util.List;
import java.util.Optional;

public interface IBookingService {
    Response createBooking(Booking booking);

    Response getBooking(Integer id);

    Response updateBooking(Booking booking, Integer id, String token);

    Response getBookingIds();
    Response deleteBooking(Integer id, String token);
}
