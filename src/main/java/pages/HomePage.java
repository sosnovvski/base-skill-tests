package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;

public class HomePage extends BasePage implements IPage {
    @FindBy(className = "oxd-main-menu-item--name")
    private List<WebElement> leftMenuItems;

    public HomePage(WebDriver driver) {
        super(new WebDriverWait(driver, Duration.ofSeconds(10)), driver);
        PageFactory.initElements(driver, this);
    }

    @Override
    public boolean isPageDisplayed() {
        return webDriverWait.until(webDriver -> leftMenuItems.size() > 0);
    }

    public TimePage clickLeftMenuItem(String lefMenuItemName) {
        WebElement leftMenuElement = leftMenuItems.stream()
                .filter(webElement -> webElement.getText().equals(lefMenuItemName))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Left menu item not found"));
        leftMenuElement.click();
        return new TimePage(driver);
    }
}
