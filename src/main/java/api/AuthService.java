package api;

import api.model.Credential;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import lombok.SneakyThrows;

import static io.restassured.RestAssured.given;

public class AuthService implements IAuthService {
    private static final String BASE_URL = "https://restful-booker.herokuapp.com";
    ObjectMapper mapper;

    public AuthService() {
        RestAssured.baseURI = BASE_URL;
        this.mapper = new ObjectMapper();
    }

    @Override
    @SneakyThrows
    public String getToken() {
        Credential credential = Credential.builder().username("admin").password("password123").build();
        return given()
                .contentType(ContentType.JSON)
                .log().uri()
                .log().body()
                .log().method()
                .body(mapper.writeValueAsString(credential))
                .post("/auth")
                .then()
                .extract().response().getBody().jsonPath().get("token");
    }
}
