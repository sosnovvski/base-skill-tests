package api;

import api.model.Booking;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import org.hamcrest.Matchers;

import static io.restassured.RestAssured.given;

public class BookingService implements IBookingService {
    private static final String BASE_URL = "https://restful-booker.herokuapp.com";
    private static final Long MAX_TIMEOUT = 6000L;
    ObjectMapper mapper = new ObjectMapper();


    public BookingService() {
        RestAssured.baseURI = BASE_URL;
        ResponseSpecBuilder resBuilder = new ResponseSpecBuilder();
        resBuilder.expectResponseTime(Matchers.lessThan(MAX_TIMEOUT));
        RestAssured.responseSpecification = resBuilder.build();
    }

    @Override
    @SneakyThrows
    public Response createBooking(Booking booking) {
        return given()
                .contentType(ContentType.JSON)
                .log().uri()
                .log().body()
                .log().method()
                .body(mapper.writeValueAsString(booking))
                .post("/booking")
                .then()
                .extract().response();
    }

    @Override
    public Response getBooking(Integer id) {
        return given()
                .contentType(ContentType.JSON)
                .log().uri()
                .log().body()
                .log().method()
                .pathParam("bookingId", String.valueOf(id))
                .get("/booking/{bookingId}")
                .then()
                .extract().response();
    }

    @Override
    @SneakyThrows
    public Response updateBooking(Booking booking, Integer id, String token) {
        return given()
                .contentType(ContentType.JSON)
                .log().uri()
                .log().body()
                .log().method()
                .cookie("token", token)
                .pathParam("bookingId", String.valueOf(id))
                .body(mapper.writeValueAsString(booking))
                .put("/booking/{bookingId}")
                .then()
                .extract().response();
    }

    @Override
    public Response getBookingIds() {
        return given()
//                .contentType(ContentType.JSON)
                .log().uri()
                .log().body()
                .log().method()
//                .pathParam("bookingId", String.valueOf(id))
                .get("/booking")
                .then()
                .extract().response();
    }

    @Override
    public Response deleteBooking(Integer id, String token) {
        return given()
                .contentType(ContentType.JSON)
                .log().uri()
                .log().body()
                .log().method()
                .cookie("token", token)
                .pathParam("bookingId", String.valueOf(id))
                .delete("/booking/{bookingId}")
                .then()
                .extract().response();
    }
}
