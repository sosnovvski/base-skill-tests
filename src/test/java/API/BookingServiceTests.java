package API;

import api.IBookingService;
import api.RestService;
import api.model.Booking;
import api.model.BookingDates;
import io.restassured.response.Response;
import lombok.extern.java.Log;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.NoSuchElementException;

@Log
public class BookingServiceTests {
    @Test(description = " Create new booking entry with your name – verify booking created with provided details in request and print the response")
    public void givenNewBookingRequestPerformedThenActionPerformedProperly() {
        Booking booking = Booking.builder()
                .firstName("Kamil")
                .lastName("Sosnowski")
                .totalPrice(111)
                .bookingDates(BookingDates.builder()
                        .checkIn("2018-01-01")
                        .checkOut("2019-01-01")
                        .build())
                .additionalNeeds("Breakfast")
                .depositPaid(false)
                .build();
        IBookingService bookingService = RestService.getRestService().getBookingService();
        Response response = bookingService.createBooking(booking);
        Assert.assertEquals(response.statusCode(), 200, "Wrong HTTP code");
        response = bookingService.getBooking(response.jsonPath().getInt("bookingid"));
        Assert.assertEquals(response.statusCode(), 200, "Wrong HTTP code");
        Assert.assertEquals(response.jsonPath().getString("lastname"), "Sosnowski", "Wrong last name from booking service");
        response.body().prettyPrint();
    }

    @Test(description = "Get newly created booking entry – verify it’s possible to retrieve the booking and print the response")
    public void givenTheNewestBookingEntryThenVerifyExists() {
        IBookingService bookingService = RestService.getRestService().getBookingService();
        Response response = bookingService.getBookingIds();
        Assert.assertEquals(response.statusCode(), 200, "Wrong HTTP code");
        Integer maxIdValue = response.jsonPath().getList("bookingid").stream().mapToInt(v -> (int) v)
                .max().orElseThrow(NoSuchElementException::new);
        log.info("Last bookingId:" + maxIdValue);
        response = bookingService.getBooking(maxIdValue);
        Assert.assertEquals(response.statusCode(), 200, "Wrong HTTP code");
        response.body().prettyPrint();
    }

    @Test(description = "Update your booking with any information you would like to - verify booking was updated with provided data and print the response")
    public void givenUpdatedRandomRecordThenRecordShouldBeChanged() {
        Booking booking = Booking.builder()
                .firstName("Kamil")
                .lastName("Sosnowski")
                .totalPrice(111)
                .bookingDates(BookingDates.builder()
                        .checkIn("2018-01-01")
                        .checkOut("2019-01-01")
                        .build())
                .additionalNeeds("Breakfast")
                .depositPaid(false)
                .build();
        IBookingService bookingService = RestService.getRestService().getBookingService();
        Response response = bookingService.createBooking(booking);
        Assert.assertEquals(response.statusCode(), 200, "Wrong HTTP code");

        booking.setFirstName("Kamil - The new one");
        String token = RestService.getRestService().getAuthService().getToken();
        response = bookingService.updateBooking(booking, response.jsonPath().getInt("bookingid"), token);
        Assert.assertEquals(response.statusCode(), 200, "Wrong HTTP code");
        Assert.assertEquals(response.jsonPath().getString("firstname"), "Kamil - The new one", "Wrong last name from booking service");
        response.body().prettyPrint();
    }


    @Test(description = "Delete your booking - verify booking was deleted and print the response")
    public void givenNewlyCreatedBookingWhenBookingHasBeenDeletedThenBookingNotFound() {
        Booking booking = Booking.builder()
                .firstName("Kamil")
                .lastName("Sosnowski")
                .totalPrice(111)
                .bookingDates(BookingDates.builder()
                        .checkIn("2018-01-01")
                        .checkOut("2019-01-01")
                        .build())
                .additionalNeeds("Breakfast")
                .depositPaid(false)
                .build();
        IBookingService bookingService = RestService.getRestService().getBookingService();
        Response response = bookingService.createBooking(booking);
        Assert.assertEquals(response.statusCode(), 200, "Wrong HTTP code");

        String token = RestService.getRestService().getAuthService().getToken();
        Integer id = response.jsonPath().getInt("bookingid");
        response = bookingService.deleteBooking(id, token);
        Assert.assertEquals(response.statusCode(), 201, "Wrong HTTP code");
        response.body().prettyPrint();

        response = bookingService.getBooking(id);
        Assert.assertEquals(response.statusCode(), 404, "Wrong HTTP code");
    }
}
