package GUI;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import pages.TimePage;
import pages.TimesheetPage;

public class OpenSourceTests {
    private static final ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    @BeforeMethod
    public void setUpBrowser() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("disable-gpu");
        driver.set(new ChromeDriver(chromeOptions));
    }

    @Test(description = "This test validates comment section in timesheet")
    public void givenLogInToTheAppWhenProvideTheCommentThenCommentPresentedProperly() {
        LoginPage loginPage = new LoginPage(driver.get());
        loginPage.openLoginPage();
        loginPage.isPageDisplayed();
        loginPage.fillUsername("Admin");
        loginPage.fillPassword("admin123");
        HomePage homePage = loginPage.submitLoginForm();
        homePage.isPageDisplayed();
        TimePage timePage = homePage.clickLeftMenuItem("Time");
        timePage.isPageDisplayed();
        TimesheetPage timesheetPage = timePage.viewTimesheetForGiverPeriod("2022-08-15 - 2022-08-21");
        timesheetPage.isPageDisplayed();
        timesheetPage.clickCommentIcon();
        Assert.assertEquals(timesheetPage.fetchCommentText(), "Leadership Development", "Comment not presented properly!");
    }

    @Test(description = "This test validates negative login")
    public void givenProvidedWrongPasswordThenUserCanNotLogIn() {
        LoginPage loginPage = new LoginPage(driver.get());
        loginPage.openLoginPage();
        loginPage.isPageDisplayed();
        loginPage.fillUsername("Admin");
        loginPage.fillPassword("wrongpass");
        loginPage.submitLoginForm();
        Assert.assertEquals(loginPage.fetchAlertText(), "Invalid credentials", "Invalid credentials alert not presented properly!");
    }

    @AfterMethod
    public void teardown() {
        driver.get().quit();
    }
}
