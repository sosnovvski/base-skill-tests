package api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookingDates {
    @JsonProperty("checkin")
    String checkIn;
    @JsonProperty("checkout")
    String checkOut;
}
