package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class LoginPage extends BasePage implements IPage {
    private static final String LOGIN_PAGE = "https://opensource-demo.orangehrmlive.com/";

    @FindBy(xpath = "//input[@name='username']")
    private WebElement usernameInput;
    @FindBy(xpath = "//input[@name='password']")
    private WebElement passwordInput;
    @FindBy(xpath = "//button[@type='submit']")
    private WebElement submit;
    @FindBy(className = "oxd-alert-content-text")
    private WebElement alertTextBox;

    public LoginPage(WebDriver driver) {
        super(new WebDriverWait(driver, Duration.ofSeconds(10)), driver);
        PageFactory.initElements(driver, this);
    }


    @Override
    public boolean isPageDisplayed() {
        return webDriverWait.until(webDriver -> usernameInput.isDisplayed());
    }

    public void openLoginPage() {
        driver.navigate().to(LOGIN_PAGE);
    }

    public void fillUsername(String username) {
        usernameInput.sendKeys(username);
    }

    public void fillPassword(String password) {
        passwordInput.sendKeys(password);
    }

    public String fetchAlertText() {
        webDriverWait.until(ExpectedConditions.visibilityOf(alertTextBox));
        return alertTextBox.getText();
    }

    public HomePage submitLoginForm() {
        submit.click();
        return new HomePage(driver);
    }
}
