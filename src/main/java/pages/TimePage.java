package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;

public class TimePage extends BasePage implements IPage {
    @FindBy(className = "oxd-table-card")
    private List<WebElement> timesheetRecords;

    public TimePage(WebDriver driver) {
        super(new WebDriverWait(driver, Duration.ofSeconds(10)), driver);
        PageFactory.initElements(driver, this);
    }

    @Override
    public boolean isPageDisplayed() {
        return webDriverWait.until(webDriver -> timesheetRecords.size() > 0);
    }

    public TimesheetPage viewTimesheetForGiverPeriod(String period) {
        WebElement timesheetRecord = timesheetRecords.stream()
                .filter(webElement -> webElement.getText().contains(period))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Period timesheet item not found"));
        timesheetRecord.findElement(By.xpath(".//button")).click();
        return new TimesheetPage(driver);
    }
}
