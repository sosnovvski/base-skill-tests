package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    public BasePage(WebDriverWait webDriverWait, WebDriver driver) {
        this.webDriverWait = webDriverWait;
        this.driver = driver;
    }

    protected WebDriverWait webDriverWait;
    protected WebDriver driver;
}
